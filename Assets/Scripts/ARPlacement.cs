using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Timeline;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPlacement : MonoBehaviour
{
    public GameObject placementIndicator;
    public ARRaycastManager aRRaycastManager;
    public ARPlaneManager aRPlaneManager;
    public Camera arCamera;
    
    [HideInInspector]public static Pose PlacementPose;
    [SerializeField] private bool usePlacementIndicator = false;
    private bool _placementPoseIsValid = false;

    //[SerializeField] private GameObject tempGhostObject = null;
    //[SerializeField] private GameObject tempObject = null;
    
    private GameObject _ghostObject; // holder for transparent game object for placement visualisation
    public GameObject[] placementConformationButtons;
    
    public float rotateSpeedModifier = 0.1f;
    private string _activeProductSku; // product SKU to instantiate

    public static event Action GhostActive;
    public static event Action GhostInactive;
    private void Start()
    {
        PanelController.ShopPanel += HandleShopPanel;
        UpdateConfirmButtons();
    }

    private void OnDestroy()
    {
        PanelController.ShopPanel -= HandleShopPanel;
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();
        if (_ghostObject != null) UpdateWorldObject(_ghostObject);

        foreach (var plane in aRPlaneManager.trackables)
            plane.gameObject.SetActive(false);
    }

    private void UpdatePlacementIndicator()
    {
        if (_placementPoseIsValid)
            placementIndicator.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
        
        placementIndicator.SetActive(usePlacementIndicator && _placementPoseIsValid);
    }

    private void UpdatePlacementPose()
    {
        var screenPosition = arCamera.ViewportToScreenPoint(new Vector3(0.5f, 0.6f));
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        aRRaycastManager.Raycast(screenPosition, hits, TrackableType.Planes); // only want to detect planes

        _placementPoseIsValid = hits.Count > 0;
        if (_placementPoseIsValid) PlacementPose = hits[0].pose;
    }
    
    void UpdateWorldObject(GameObject objectToUpdate)
    {
        objectToUpdate.transform.position = PlacementPose.position;
        
        if (Input.touchCount == 0) return;
        
        var touch = Input.GetTouch(0);

        if (touch.phase != TouchPhase.Moved) return;
        Quaternion rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * rotateSpeedModifier, 0f);
        objectToUpdate.transform.rotation = rotationY * objectToUpdate.transform.rotation;
        
    }

    void HandleShopPanel(bool panelStatus)
    {
        usePlacementIndicator = panelStatus;
    }

    public void SpawnGhostObject(string product)
    {
        DestroyGhostObject();

        var loadedPrefabResource = LoadPrefabFromFile(product+"ghost");
        _activeProductSku = product;
        _ghostObject = (GameObject) Instantiate(loadedPrefabResource, PlacementPose.position, PlacementPose.rotation);
        
        GhostActive?.Invoke(); // other world object will not respond to touch while ghost is active
        UpdateConfirmButtons();
    }

    public void SpawnRealObject()
    {
        if (_ghostObject == null) return;
        var loadedPrefabResource = LoadPrefabFromFile(_activeProductSku);
        var newObject = (GameObject)Instantiate(loadedPrefabResource, _ghostObject.transform.position, _ghostObject.transform.rotation);
        DestroyGhostObject();
        GhostInactive?.Invoke();
    }

    void UpdateConfirmButtons()
    {
        foreach (var GO in placementConformationButtons)
        {
            GO.SetActive(_ghostObject != null);
        }
    }

    public void DestroyGhostObject()
    {
        if (_ghostObject == null) return;
        Destroy(_ghostObject);
        _ghostObject = null;
        
        UpdateConfirmButtons();
    }
    
    private UnityEngine.Object LoadPrefabFromFile(string filename)
    {
        Debug.Log("Trying to load LevelPrefab from file ("+filename+ ")...");
        var loadedObject = Resources.Load("Prefabs/" + filename);
        if (loadedObject == null)
        {
            throw new FileNotFoundException("...no file found - please check the configuration");
        }
        return loadedObject;
    }
}
