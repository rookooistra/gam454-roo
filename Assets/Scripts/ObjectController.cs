using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ObjectController : MonoBehaviour
{
    public static event Action<GameObject> NewObject;
    public static event Action<GameObject> RemoveObject;
    
    //private Touch touch;
    //private Quaternion rotationY;
    private float rotateSpeedModifier = 0.1f;

    private bool _ghostActive = false;
    private bool _isActive = false;

    [SerializeField] GameObject isActiveGameObject; // extras to switch on when main gameobject isActive

    private void Start()
    {
        ARPlacement.GhostActive += HandleGhostActive;
        ARPlacement.GhostInactive += HandleGhostInactive;
    }

    private void OnEnable()
    {
        NewObject?.Invoke(this.gameObject);
    }

    private void OnDestroy()
    {
        RemoveObject?.Invoke(this.gameObject);
        
        ARPlacement.GhostActive -= HandleGhostActive;
        ARPlacement.GhostInactive -= HandleGhostInactive;
    }

    void HandleGhostActive()
    {
        _ghostActive = true;
    }

    void HandleGhostInactive()
    {
        _ghostActive = false;
    }

    private void Update()
    {
        if (_ghostActive) _isActive = false;
        
        isActiveGameObject.SetActive(_isActive);
        if (!_isActive) return;
        
        // lerp movement
        var smoothedPosition = Vector3.Lerp(transform.position, ARPlacement.PlacementPose.position, Time.deltaTime * 5f);
        transform.position = smoothedPosition;
        
        if (Input.touchCount == 0 || _ghostActive) return;
        
        var touch = Input.GetTouch(0);

        if (touch.phase != TouchPhase.Moved) return;
        Quaternion rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * rotateSpeedModifier, 0f);
        transform.rotation = rotationY * transform.rotation;
    }

    public void ToggleIsActive()
    {
        _isActive = !_isActive;
    }
}
